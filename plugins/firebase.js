import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyDucpjDJWDxWThISyEJ9Hubyt1pv6cbozc",
  authDomain: "event-mng.firebaseapp.com",
  databaseURL: "https://event-mng.firebaseio.com",
  projectId: "event-mng",
  storageBucket: "event-mng.appspot.com",
  messagingSenderId: "61615945124",
  appId: "1:61615945124:web:b1a661f373504400ac7d31",
  measurementId: "G-63XC6YTZ4T"
};
// Initialize Firebase
!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : ''
export const auth = firebase.auth()
export default firebase