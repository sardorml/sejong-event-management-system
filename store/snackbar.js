export const state = () => ({
    message: null,
    color: null
})

export const getters = {
    message: state => state.message,
    color: state => state.color
}

export const mutations = {
    successMessage: (state,message) => {
        state.message = message
        state.color = 'success'
    },
    errorMessage: (state,message) => {
        state.message = message
        state.color = 'error'
    },
    message: (state,message) => state.message = message.action,
    color: (state,color) => state.color = color
}

export const actions = {
    
}