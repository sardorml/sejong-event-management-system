import firebase from '@/plugins/firebase'

export const state = () => ({
  user: '',
  userDetails: null,
  token: null
})

export const getters = {
  user: (state) => process.browser 
  ? (JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')) : state.user)
  : state.user,
  userDetails: (state) => state.userDetails
}

export const mutations = {
  setUser: (state,user) => state.user = user,
  setUserDetails: (state,details) => state.userDetails = details
}

export const actions = {
  async register({commit},{email,password,info}){
    try{
      let data = await firebase.auth().createUserWithEmailAndPassword(email, password)
      console.log('data is',data)
      let docRef = await firebase.firestore().collection("users").doc(data.user.uid).set({
        uid: data.user.uid,
        payload: info
      })
      console.log("Document written with ID: ", docRef);
      commit('setUser',{
        email: data.user.email,
        uid: data.user.uid
      })
      localStorage.setItem('user',JSON.stringify({
        email: data.user.email,
        uid: data.user.uid
      }))
      return 'Successful'
    } catch(err){
      console.log(err)
      throw new Error(err)
    }
  },
  async login({commit},{email,password}){
    try{
      let data = await firebase.auth().signInWithEmailAndPassword(email, password)
      console.log('data is',data)
      commit('setUser',{
        email: data.user.email,
        uid: data.user.uid
      })
      localStorage.setItem('user',JSON.stringify({
        email: data.user.email,
        uid: data.user.uid
      }))
      return 'Successful'
    } catch(err){
      console.log(err)
      throw new Error(err)
    }
        
  },
  async getUserInfo({commit}, uid){
    try{
      console.log('uid is', uid)
      let docRef = await firebase.firestore().collection("users").doc(uid).get()
      if (docRef.exists) {
          commit('setUserDetails',docRef.data())
      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }

    } catch(err){
      console.error("Error reading document: ", err);
    }
  }
}