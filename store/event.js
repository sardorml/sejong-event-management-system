import firebase from '@/plugins/firebase'

export const state = () => ({
  events: null,
  userEvents: null,
  buildings: null
})

export const getters = {
  events: state => state.events,
  userEvents: state => state.userEvents,
  buildings: state => state.buildings,
}

export const mutations = {
  setEvents: (state,events) => state.events = events,
  setUserEvents: (state,userEvents) => state.userEvents = userEvents,
  setBuildings: (state,buildings) => state.buildings = buildings,
}

export const actions = {
  async getUserEvents({commit}){
    try{
      let querySnapshot = await firebase.firestore().collection("events").where("user.uid", "==", JSON.parse(localStorage.getItem('user')).uid).get()
      let events = []
      querySnapshot.forEach(function(doc) {
          console.log(doc.id, " => ", doc.data());
          events.push({
            id: doc.id,
            data: doc.data()
          })
      });
      commit('setUserEvents', events)
    } catch(err){
      console.error("Error reading document: ", err);
    }
  },
  async getEvents({commit}){
    try{
      let querySnapshot = await firebase.firestore().collection("events").get()
      let events = []
      querySnapshot.forEach(function(doc) {
          console.log(doc.id, " => ", doc.data());
          events.push({
            id: doc.id,
            data: doc.data()
          })
      });
      commit('setEvents', events)
    } catch(err){
      console.error("Error reading document: ", err);
    }
  },
  async getBuildings({commit}){
    try{
      let querySnapshot = await firebase.firestore().collection("buildings").get()
      let buildings = []
      querySnapshot.forEach(function(doc) {
          console.log(doc.id, " => ", doc.data());
          buildings.push({
            id: doc.id,
            data: doc.data()
          })
      });
      commit('setBuildings', buildings)
    } catch(err){
      console.error("Error reading document: ", err);
    }
  },
  async getEventsByBuilding({},building){
    try{
      let querySnapshot = await firebase.firestore().collection("events").where('building', '==' ,building).get()
      let events = []
      querySnapshot.forEach(function(doc) {
          console.log(doc.id, " => ", doc.data());
          events.push({
            id: doc.id,
            data: doc.data()
          })
      });
      return events
    } catch(err){
      console.error("Error reading document: ", err);
      throw new Error(err)
    }
  },
  async createEvent({commit, rootState},payload){
    try{
      payload['user'] = JSON.parse(localStorage.getItem('user'))
      let docRef = await firebase.firestore().collection("events").add(payload)
      console.log("Document written with ID: ", docRef.id);
      commit('snackbar/successMessage','Event has been added successfully',{root:true})
    } catch(err){
      console.error("Error adding document: ", err);
    }
  }
}