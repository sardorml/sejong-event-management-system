export default function ({ store, redirect }) {
  if(!process.server){
    let user = JSON.parse(localStorage.getItem('user'))
    console.log('authed',user)
    // If the user is not authenticated
    if (!user) {
      return redirect('/login')
    }
  }
}